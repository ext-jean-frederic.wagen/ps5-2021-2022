---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: BackUp5G - smart QoS based backhaul for a temporary 4/5G base station
filières:
  - Télécommunications
  - Informatique
  - ISC
nombre d'étudiants: 2
mots-clés:
  - QoS
  - bonded links
  - 5G backhaul
  - security in mobile network
  - software defined network
  - gns3
  - cisco
  - mikrotik
mandants:
  - Swisscom 
langue: [F,E,D]
confidentialité: non
suite: oui
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/backUp5G.jpg}
\end{center}
```

## Description/Contexte

Objectif

Design, build and test a system offering redundant and/or bonded links between a 4/5G base station and the core network (= backhaul link) with a maximum possible quality and security. The end-to-end quality cannot be guaranteed but should be offered as a "very best effort" based on the available (possibly changing) QoS characteristics of the available backhaul link(s). Currently only a single VDSL link is being tested which might not be sufficient in some cases. 60GHz or WiFi point-to-point links to "private" fiber access might be of interest.  

Due to the complexity of this projet and to mimic Swisscom working procedures, this P5 project will use an agile development methodology. The first sprint will be to define the tasks according to the current knowledge and interest of the candidate(s).


## Objectifs/Tâches

- Analyse du projet et propositions
- Choix des technologies pour ce projet
- Développement de la/les partie(s) proposées
- Tests et validation
- Rédaction d'une documentation valorisant le travail réalisé et permettant le transfert de connaissance vers les ingénieurs du mandant.