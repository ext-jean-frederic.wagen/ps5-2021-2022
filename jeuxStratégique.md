---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2021/2022
titre: Conception d’un jeu de stratégie (human vs machine)
filières:
  - Informatique
  - ISC
nombre d'étudiants: 1
mots-clés: [Reinforcement Learning, OpenAI, mini-metro, distribution de vaccins]
mandants: 
- Yann Maret (doctorant)
langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/jeuxS.jpg}
\end{center}
```

## Description/Contexte

“Mini-métro” ou plus récent “mini-motorway” sont des jeux de stratégie en design d’infrastructures. Dans le cas du jeu des métros, il est question de stratégiquement placer des connections entre les stations de métros pour acheminer les passagers représentés par des formes aux couleurs variées aux destinations appropriées. Dans la version motorway des routes remplacent les voix de métro.  
Le jeu est composé de ressources et de tâches (ou missions) à accomplir. Les ressources permettent de réaliser les tâches qui apparaissent aléatoirement durant la partie. Au fur et à mesure que le joueur évolue dans le jeu, des ressources sont débloquée pour supporter une plus grande quantité de tâches.
Le but du jeu est d’observer une bonne stratégie dans la gestion des ressources de façon à effectuer le plus de tâches tout en maintenant une fluidité du trafic. Pensez-vous pouvoir relever le défi et de garantir un trafic fluide en fonction de la demande ?

L’allocation des ressources est un problème bien connu et présent dans beaucoup de domaines, mini-métro illustre un parfait exemple de problème de logistique. 
Dans ce projet il est question de concevoir un programme ludique en 2D doté de ressources et de missions sur la base d’un environnement OpenAI’s GYM. Les ressources seront limitées à une certaine quantité mais disponible à une réorganisation intemporelle. Les missions doivent être connues à l’avance. De plus, le jeu devra entre autres respecter la structure d’une classe d’un environnement GYM.
L'apprentissage par renforcement (RL) sera testé avec la suite d’agents qu’offre la librairie stable-baselines (par exemple).

## Objectifs/Tâches

- Planifier la liste des ressources et des missions
- Investiguer la librairie Serpent.AI et stable-baselines
- Modéliser le jeu et les agent RL (UML)
- Concevoir le jeu en 2D en python (pygame, ...)

