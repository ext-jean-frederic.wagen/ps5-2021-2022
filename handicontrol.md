---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2021/2022
titre: HandiControl - machine learning based home control for polyhandicap
filières:
  - Informatique
  - ISC
nombre d'étudiants: 2
mots-clés:
  - machine learning
  - vision par ordinateur
  - mobile application
  - Android
  - desktop app
  - smart watch
  - smart sensor
mandants:
  - Home Atelier La Colline
langue: [F,E,D]
confidentialité: non
suite: oui
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/HandiControlTeaserPhoto.jpg}
\end{center}
```

## Description/Contexte

Améliorer l'apprentissage d'une certaine autonomie pour des personnes fortement polyhandicappées. Voir `https://www.handicontrol.com/`, rêver et appliquer les technologies récentes de manière innovante.

Dans le contexte décrit ci-dessous, le projet vise la conception, le développement et les tests d'un prototype de reconnaissance de son, et/ou d'image (expression), de mouvement (vidéo et/ou capteurs), de signaux d'une montre connectée, etc., qui permet à Monsieur F. (polyhandicappé) d'interagir directement avec son environnement et de gérer, dans son temps libre, la musique, la lumière, grâce à des systèmes de domotique. On pense que son handicap ne lui permet que de choisir entre 2 propositions; par exemple, il pourra choisir s'il veut continuer ou pas à écouter de la musique en exprimant son appréciation ou, au contraire, son mécontentement. Mais des indices montrent qu’il pourrait faire bien plus … Aidez-nous à le découvrir.

Le système devrait être adaptatif pour pouvoir intégrer de nouveaux sons ou d’autres modalités permettant à la reconnaissance automatique de pouvoir évoluer avec les compétences (qui restent à être découvertes) de Monsieur F.

Présentation de la personne et du contexte:
Monsieur F. est un homme de 40 ans. Il vit depuis 1997 dans la même institution, qui accompagne des personnes adultes en situation de handicap. Très souriant et spontané, il a toujours vécu avec un polyhandicap qui ne lui a pas permis l'accès à la parole et qui limite beaucoup tous ses mouvements. Il se déplace à l'aide d'un fauteuil roulant, adapté à la forme de son corps. 


Communication

Au fil des années, il a su créer son propre langage et actuellement il 
communique avec son entourage avec des bruits et mimiques 
différents. Avec son vocabulaire, il peut nous exprimer son 
appréciation / mécontentement par rapport à une 
activité/goût/situation, ainsi que nous communiquer s'il a faim et soif. 
L'interprétation du contexte, des mimiques, gestes spontanés et 
réponses émotionnelles qu'il propose, permettent à l'entourage de 
répondre à d'autres besoins (par exemple le sommeil, le confort). Sa 
façon de communiquer est donc un mélange de signaux intentionnels 
et non intentionnels. Ceux-ci nécessitent toujours l'interprétation d'un 
intermédiaire pour qu'il puisse agir sur son environnement. 


Moyens auxiliaires

Monsieur F. a utilisé différents moyens auxiliaires lui permettant 
d'accéder à certaines activités et de faire un lien cause-effet entre ses 
mouvements et le déclenchement d'un stimulus. Par le passé, il avait 
un instrument (Carba) avec lequel il enclenchait la musique en levant la 
tête. Dès qu'il posait la tête, la musique s'arrêtait. Mais cet outil lui 
coûtait beaucoup d'effort et la position avec la tête levée, était pour lui 
inconfortable. Actuellement, il utilise un interrupteur adapté 
(contacteur), qui lui permet d'allumer des appareils électroménagers 
en maintenant l'appui sur le switch. Mais celui-ci ne fonctionne pas 
avec les appareils de nouvelle génération, ce qui est très limitant. 

Collaboration

L'équipe éducative croit beaucoup en ce projet, qui pourrait influencer la qualité de vie de Monsieur F. de façon très positive. Elle est donc très ouverte à la collaboration en tout au long du projet. 
Des rencontres à la HEIA-FR ou au Home Atelier La Colline seront organisées (on-line bien sûr selon les règles qui conviendront).

Données 

Une banque de données audio et vidéo confidentielles sera mise à votre disposition, ainsi que l'interprétation des données par les éducateurs. Les codes et documents actuels sont par contre en open source et votre contribution devra être publié en open source.

Pour le groupe Persée - Home Atelier La Colline, 1670 Ursy
D. Dubé

## Objectifs/Tâches

- Analyse du projet et proposition d’amélioration
- Choix des technologies pour ce projet
- Développement de la/les partie(s) proposées
- Tests et validation
- Rédaction d'une documentation valorisant le travail réalisé.
- Publication en open source