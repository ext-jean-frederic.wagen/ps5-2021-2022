---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2021/2022
titre: SmartNurse - off/online Mobile App
filières:
  - Informatique
  - Télécommunications
  - ISC
nombre d'étudiants: 2
mots-clés:
  - mobile application
  - mobile database
  - server application
  - database server
  - Android et/ou iOS
  - database replication, backup, synchronization
mandants:
  - Swissmedic
langue: [F,E,D]
confidentialité: non
suite: oui
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/smartNurse.jpg}
\end{center}
```

## Description/Contexte

Objectif

Le système de gestion des prestations envisagé dans ce projet a pour objectif de révolutionner la saisie des prestations par les infirmières lors des consultations auprès de leurs patients.

Introduction

Cet objectif ambitieux permet de découper le projet complets en plusieurs tâches intéressantes, adaptée à vos intérêts et, bien sûr, à la durée d'un projet de semestre S5. Ainsi, les objectifs et tâches pour ce projet de semestre seront définis lors de la première semaine du projet en collaboration avec le mandant : M. Lehner (imedic).
La description ci-dessous concerne l'ensemble du projet SmartNurse avec les premiers objectifs pour ce projet de semestre.

Le contexte du projet SmartNurse est le suivant:

La saisie des heures et des prestations par les infirmières pour leurs patients sont des tâches essentielles et qui imposent une charge administrative importante, notamment en raison des prestations fournies, des déplacements ou encore des formulaires cantonaux à fournir en vue d'obtenir un remboursement. 

Des prototypes de SmartNurse, permettant d'alléger les tâches administratives d'une infirmière, ont été développés.

Le problème actuel est que la banque de données utilisée par les fournisseurs de soins est distribuée sur chaque appareil mobile. La centralisation des données des bases de données embarquées de chaque appareil utilisant l'application devient nécessaire. Pour cela il faudrait concevoir une seconde base pour répliquer puis fusionner ces données en donnant à l'utilisateur le contrôle complet de la gestion des éventuels conflits. 

Une réplication des données est un service indispensable si l'application veut être fiable et utilisable online et offline (pour des raisons de protection des données et de sécurité). En effet si le smartphone d'une infirmière venait à être inutilisable, toutes les données des patients, des interventions enregistrées et des prestations fournies seraient perdues. C'est pourquoi une seconde base de données est primordiale. Celà implique (1) une sécurisation des données jusqu'au serveur dédié, et (2) un système fiable et pratique pour la gestion de conflits possibles liés à la réplication et les modifications par les utilisateurs.


## Objectifs/Tâches

- Analyse du projet et propositions
- Choix des technologies pour ce projet
- Développement de la/les partie(s) proposées
- Tests et validation
- Rédaction d'une documentation valorisant le travail réalisé et permettant le transfert de connaissance vers le mandant.