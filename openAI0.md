---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2021/2022
titre: OpenAI0 Agent intelligent pour orchestrer des réseaux sans fil
filières:
  - Télécommunications
  - Informatique
  - ISC
nombre d'étudiants: 2
mots-clés:
  - OpenAI
  - Machine Learning
  - Reinforcement Learning
  - Simulation based Training
  - Wireless Mobile Area Network
  - Vehicle to Vehicle Network
mandants:
  - armasuisse
langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/openAIorchestration.jpg}
\end{center}
```

## Description/Contexte

Introduction :

Les réseaux mobiles pour la protection de la population, les opérations tactiques, les urgences ainsi que pour les véhicules autonomes se basent sur des systèmes radios avec (V2I) ou sans (V2V ou MANET) infrastructure. Le rôle du Machine Learning et du Reinforcement Learning (RL) est devenu indispensable pour améliorer les performances. Le but de ce projet est de préparer un TP permettant de visualiser les bases du RL pour MANET. 

Contexte :

Mise en place d’un agent par apprentissage automatique pour réseau communicant sans-fil afin d’orchestrer au mieux les nœuds émettant des informations et relayant des données. 
Un environnement comprenant des réseaux virtuels est mis à disposition pour les tests. Les réseaux sans-fil sont virtualisés et simulés pour faciliter les déploiements et les tests. Un réseau comporte des nœuds qui peuvent envoyer, recevoir et retransmettre l’information, ces nœuds sont placés sur un terrain à des emplacement précis. Le réseau devra supporter une charge de traffic depuis des nœuds sources.

L’agent sera implémenté avec la libraire OpenAI qui offre un panel de modèles pour l’apprentissage automatique et notamment des modèles pour le Renforcement Learning. L’agent sera capable de mettre en place un simple schedule pour accorder le temps de parole aux différents nœuds de l’environnement.

## Objectifs :
- Compréhension de la simulation et de la librairie OpenAI
- Mise en place d’un agent RL: l’agent devra, compte tenu de la simulation, trouver un/le meilleur schedule possible
- Entrainer l’agent. Tester et évaluer l'agent dans son environnement appris. Et valider les performances de l'agent dans des cas non connus.
- Documenter les fonctionnalités de la librairie OpenAI, les tests réalisés et les mesures faite.

Mandant:  Y. Maret (doctorant dans le domaine du Machine Learning pour MANETs)
Superviseur: J.-F. Wagen