---
version: 2
type de projet: Projet de semestre 5
année scolaire: 2020/2021
titre: cloudControlDrone - I did it because I did not know it was impossible
filières:
  - Télécommunications
  - Informatique
  - ISC
nombre d'étudiants: 2
mots-clés:
  - cloud
  - remote control
  - low latency
  - 4G/5G
  - Wireless Mobile Area Network
mandants:
  - RAINBOW SERVICES (Uasystems.com)
langue: [F,E,D]
confidentialité: non
suite: non
---
```{=tex}
\begin{center}
\includegraphics[width=0.7\textwidth]{img/cloudControlDrone.jpg}
\end{center}
```

## Description/Contexte

A company approched us with an idea ... they need a solution.
Come to see me: J.-F. Wagen C10.07 or Teams.

## Objectifs :
- Analyse du projet et proposition d'une solution partielle
- Choix des technologies pour ce projet
- Développement de la/les partie(s) proposées
- Tests et validation
- Rédaction d'une documentation valorisant le travail réalisé pour le mandant
- Au moins 2 itérations des points ci-dessus